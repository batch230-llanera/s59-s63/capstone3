import { Fragment} from 'react';
import logo from '../components/logo.png'
import owner from '../components/owner.jpeg'
import {Card, Button, Container, Col, Row} from 'react-bootstrap'
import '../App.css'


export default function Story() {
	const title = "Our Story";
	const caption = "Leddie’s Homemade Sweets has been serving home baked cakes since 2020. The owner, Leddie, loves to draw since she was young and aspired to be an architect. Despite pursuing a different career from designing houses, she kept a keen eye for art. With the family’s love for dessert and sweets, she started baking. She’s now showcasing her talent in decorating home-made cakes. Her cakes are adorned with different edible pieces. They are delectable with eye-catching ornaments. With the good-tasting flavor, Leddie’s Homemade Sweets has fulfilled hundreds of orders since it started.";
	const aim = "The business aims to provide personalized cakes to celebrate one’s uniqueness."
	const footer = "March 2023"

	return (
		<Container className="justify-content-center p-5">
		<Row className="d-block" xs={1} md={1} lg={1}>
		<Col>
		 <Card className="text-center">
		    <Card.Header style={{backgroundColor: '#A58CB3'}} >
		    <img alt="logo" src={logo} width="120" height="50" className="d-inline-block align-top"/>
		    </Card.Header>
		    <Card.Body style={{backgroundColor: '#EAE7FA'}}>
		    <Row>
		    	<Col className="col">
		    		<Card.Title>Owner</Card.Title>
		    		<img style={{ backgroundColor: '#EAE7FA'}} alt="owner" src={owner} width="250" height="350" className="d-inline-block align-top mb-5 mt-1"/>
				</Col>
			    <Col className="col">
			      <Card.Title>{title}</Card.Title>
				      <Card.Text>{caption}</Card.Text>
				      <Card.Text>{aim}</Card.Text>
				      <Card.Text>Follow us:</Card.Text>
					      <Button style={{backgroundColor: '#A16AE8',borderColor: '#A16AE8'}} className=" m-1">Instagram</Button>
					      <Button style={{backgroundColor: '#A16AE8',
					      	borderColor: '#A16AE8'}}className="m-1">Facebook</Button>
				</Col>	     
			</Row>
		    </Card.Body>
		    <Card.Footer style={{
					      	backgroundColor: '#A58CB3',
					      	borderColor: '#A58CB3'
					      }}
					      className="text-light">{footer}</Card.Footer>
		  </Card>
		</Col>
		</Row>
	</Container>
	);
	
}